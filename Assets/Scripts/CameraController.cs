﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

    public Camera Camera;
    ItemData itData;
    int ancho = 1024;
    int anchoAux;

    void Start() {
        anchoAux = Screen.width;
        Camera = GameObject.Find("Camera").GetComponent<Camera>();
    }

    void Update() {

        Vector3 pos = transform.position;

        if (itData != null) { 
            itData.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f) ;
        }
    }
  
    public void SetItemData(ItemData idat) {
        itData = idat;
    }
}

