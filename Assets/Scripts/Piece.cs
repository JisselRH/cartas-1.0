﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Piece : MonoBehaviour
{
    public int id;
    public string nombre;
    public int group;
    public List<Posicion> posiciones = new List<Posicion>();

    void Awake() {
        id = -1;
    }

    private void Start() {
    }

    public int getIDPiece() {
        return id;
    }

    public void setIDPiece(int i) {
        id = i;
    }

    public string getName() {
        return nombre;
    }

    public void setName(string n) {
        nombre = n;
    }
 

    public bool ValidMove(Piece[,] board, int x2, int y2)
    {
        return ((board[x2, y2] != null) ? false : true);
    }
}

public class Posicion {
    public int xP { get; set; }
    public int yP { get; set; }
}
