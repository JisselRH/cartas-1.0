﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;

public class ItemData : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerEnterHandler, IPointerExitHandler {
    private Camera Camera;
    public Item item;
    public int slot;

    public ControllerBoard ControllerBoard;

    private Inventory inv;
    
    private Vector2 offset;

    private Vector3 boardOffset = new Vector3(-12f, 0, -12f);

    private Vector2 mouseOver;
    private Vector2 startDrag;
    private Vector2 endDrag;
    private int x, y, id;

    public int idItem;

    GameObject inventoryPanel, tablero;
    
    bool aux,Drag;
    public int ancho = 12, alto = 12;

    float scaleSprite;
    PointerEventData eventData;


    public void Start() {
        inv = GameObject.Find("Inventory").GetComponent<Inventory>();
        inventoryPanel = GameObject.Find("Inventory Panel");
        tablero = GameObject.Find("Tablero");
        ControllerBoard = GameObject.Find("Tablero").GetComponent<ControllerBoard>();
        //tamaño inicial del sprite
        scaleSprite = 1;
        Camera = GameObject.Find("Camera").GetComponent<Camera>();
        //print(typeof(string).Assembly.ImageRuntimeVersion);
    }

    public void Update() {
        //ControllerBoard.UpdateMouseOver();
        x = (int)mouseOver.x;
        y = (int)mouseOver.y;

        if (Drag) {
            idItem = item.ID;
            //ControllerBoard.UpdateItemDrag(x, y,item);
       }    
    }
    
    //se selecciona
    public void OnBeginDrag(PointerEventData eventData) {
        Drag = true;
        if (item != null) {
            offset = eventData.position - new Vector2(this.transform.position.x, this.transform.position.y);
            this.transform.SetParent(this.transform.parent.parent);

            this.transform.position = eventData.position - offset;
            GetComponent<CanvasGroup>().blocksRaycasts = false;
            
            //obtiene la camara y envia el objeto para ajustar el tamaño
            //GameObject camara = GameObject.Find("Camera");
            Camera.GetComponent<CameraController>().SetItemData(this);
        }

    }

    
    public void OnDrag(PointerEventData eventData) {

        if (item != null) {
            this.transform.SetParent(inv.transform);
            this.transform.position = eventData.position - offset;
        }

        RaycastHit hit;
        //print("camera "+camera);
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit, 200.0f, LayerMask.GetMask("Board"))){
            mouseOver.x = (int)(hit.point.x - boardOffset.x);
            mouseOver.y = (int)(hit.point.z - boardOffset.z);
        }
        else {
            mouseOver.x = -1;
            mouseOver.y = -1;
        }

        x = (int)mouseOver.x;
        y = (int)mouseOver.y;

       //ControllerBoard.UpdateBoard();// NO QUITAR
    }

    // se suelta el item
    public void GeneratePieceA(int x, int y) {
        ControllerBoard.GeneratePiece(x, y, item.Title, item.ID);
        mouseOver.x = 0;
        mouseOver.y = 0;
    }
    
   
    public void OnEndDrag(PointerEventData eventData) {
        Drag = false;
        inv.Eliminar();

        RaycastHit hit;
        //Ray ray = camera.ScreenPointToRay(Input.mousePosition);
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit, 200.0f, LayerMask.GetMask("Board"))) {
        //if (Physics.Raycast(ray, out hit)) {
            mouseOver.x = (int)(hit.point.x - boardOffset.x);
            mouseOver.y = (int)(hit.point.z - boardOffset.z);
        }
        else {
            mouseOver.x = -1;
            mouseOver.y = -1;
        }

        x = (int)mouseOver.x;
        y = (int)mouseOver.y;
        GetComponent<CanvasGroup>().blocksRaycasts = true;
        
        GeneratePieceA(x,y);
        inv.EliminarItem(item.Title);
        ResetTam(); //coloca el tamaño original del sprite
        //GameObject camara = GameObject.Find("Camera");
        Camera.GetComponent<CameraController>().SetItemData(null); //setea a null despues de soltar el objeto para evitar bugs

    }

    public void OnPointerEnter(PointerEventData eventData) {
        //tooltip.Activate(item);
    }

    public void ResetTam() {
        //resetea el tamaño del gameobject
        //this.transform.localScale = new Vector3(1f, 1f, 1f);
    }

    public void OnPointerExit(PointerEventData eventData) {
        //tooltip.Deactivate();
    }
    
}
