﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class ControllerBoard : MonoBehaviour {
    public int cantidad = 0;
    public Camera camera;
    public Piece[,] pieces = new Piece[24, 24];
    public List<Piece> piecesAux = new List<Piece>();

    public Piece selectedPiece, selectedPiece2, p, selected;

    public Casilla[,] casillas = new Casilla[24, 24];
    public Casilla selectedCell;
 
    private GameObject Prefab,RG0,RG1,RG2,RG3,Total,panel,inv;

    public GameObject ColorCell, playButton,tablero,button;
    public Renderer rend;

    public GameObject CasillaPrefab;
    public int ancho = 24, alto = 24;

    private Vector3 boardOffset = new Vector3(-12f, 0, -12f);

    private Vector3 pieceOffset = new Vector3(0.5f, 0, 0.5f);
    private Vector3 casillaOffset = new Vector3(0.5f, -0.09f, 0.5f);

    private Vector2 mouseOver;
    private Vector2 startDrag;
    private Vector2 endDrag;

    ItemDatabase database;

    public int x, y;
    int contador = 0, G0,G1,G2,G3, total;

    public ItemData item2;

    public List<int> cantidades;
    
    public bool Drag = false;
    public bool Arrastrando = false;

    private void Start() {
        panel = GameObject.Find("PanelResultados");
        inv = GameObject.Find("Inventory Panel");
        cantidades = new List<int>();
        tablero = GameObject.Find("Tablero");
        ColorCell = GameObject.Find("ColorCell");
        item2 = GameObject.Find("Inventory").GetComponent<ItemData>();
        database = GameObject.Find("Inventory").GetComponent<ItemDatabase>();
        camera = GameObject.Find("Camera").GetComponent<Camera>();
        RG0 = GameObject.Find("G0");
        RG1 = GameObject.Find("G1");
        RG2 = GameObject.Find("G2");
        RG3 = GameObject.Find("G3");
        Total = GameObject.Find("Total");
        playButton.SetActive(false);
        panel.SetActive(false);
        GenerateBoard();
    }

    // Update is called once per frame
    void Update() {
        UpdateMouseOver();

        x = (int)mouseOver.x;
        y = (int)mouseOver.y;

       try {
            p = pieces[x, y];
        }
        catch(Exception e) {

        }
        
        if (Input.GetMouseButtonDown(0)) {
            SelectPiece(x, y);
        }

        if (Input.GetMouseButton(0)) {
            if (selectedPiece != null && Arrastrando == false) {
                UpdatePieceDrag(selectedPiece, x, y, (int)startDrag.x, (int)startDrag.y);
            }
        }

        else if (Input.GetMouseButtonUp(0)) {
            TryMove((int)startDrag.x, (int)startDrag.y, x, y);

            Arrastrando = false;

        }
        
    }
    
  
    //devuelve una pieza de la matriz pieza segun las pocisiones dadas
    public Piece GetPiece(int x, int y) {
        Piece pd = pieces[x, y];
        return pd;
    }

    //activa las casillas al arrastrar un item del teclado dependiendo del tipo de componente
    public void UpdateItemDrag(int equis, int ye, Item itemAux) {
        RaycastHit hit;
        
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 200.0f, LayerMask.GetMask("Board"))) {
                selectedCell = casillas[equis, ye];
                selectedCell.GetComponent<Renderer>().enabled = true;
            }
    }

    public void UpdateMouseOver() {

        RaycastHit hit;
        Ray ray = camera.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 200.0f, LayerMask.GetMask("Board"))){
            mouseOver.x = (int)(hit.point.x - boardOffset.x);
            mouseOver.y = (int)(hit.point.z - boardOffset.z);
        } else {
            mouseOver.x = -1;
            mouseOver.y = -1;
        }
    }

    public void UpdatePieceDrag(Piece p, int equis, int ye, int startequis, int startye) {
        Drag = true;

        RaycastHit hit;
        Ray ray = camera.ScreenPointToRay(Input.mousePosition);
        
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 200.0f, LayerMask.GetMask("Board"))) { 
            p.transform.position = hit.point + Vector3.up/2;
        }

        contador = contador + 1;
        for (int i = 0; i < ancho; i++) {
            for (int j = 0; j < alto; j++){
                selectedCell = casillas[i, j];
                //AQUI;
                selectedCell.GetComponent<Renderer>().enabled = false;
            }
        }

        selectedCell = casillas[equis, ye];
        selectedCell.ChangeColor(true, casillas, startequis, startye);
        selectedCell.GetComponent<Renderer>().enabled = true;
    }

    public void SelectPiece(int x, int y) {

        if (x < 0 || x >= 24 || y < 0 || y >= 24)
            return;

        Piece p = pieces[x, y];
        if (p != null) {
            selectedPiece = p;
            startDrag = mouseOver;
        }

    }

    public void TryMove(int x1, int y1, int x2, int y2) {

        startDrag = new Vector2(x1, y1);
        endDrag = new Vector2(x2, y2);
        selectedPiece = pieces[x1, y1];
        // Out of bounds
        if (x2 < 0 || x2 >= 24 || y2 < 0 || y2 >= 24) {

            if (selectedPiece != null) {
                MovePiece(selectedPiece, x1, y1);
            }

            startDrag = Vector2.zero;
            selectedPiece = null;
            StatusCell(x1, y1);
            return;
        }

        if (selectedPiece != null) {
            // para intercambiar casilla   
            if (endDrag == startDrag) {
                selectedCell = casillas[x1, y1];
                MovePiece(selectedPiece, x1, y1);
                startDrag = Vector2.zero;
                selectedPiece = null;
                StatusCell(x1, y1);
                return;
            }

            // Check if its a valid move
            if (selectedPiece.ValidMove(pieces, x2, y2)) {
                BreakFree(x1, y1);
                MovePiece(selectedPiece, x2, y2);
                pieces[x2, y2] = selectedPiece;
                pieces[x1, y1] = null;
                StatusCell(x2, y2);
                selectedCell = casillas[x2, y2];

                int newNumber = x2 / 6;

                switch (newNumber) {
                    case 0:
                        G0++;
                        selectedPiece.group = 0;
                        break;
                    case 1:
                        G1++;
                        selectedPiece.group = 1;
                        break;
                    case 2:
                        G2++;
                        selectedPiece.group = 2;
                        break;
                    case 3:
                        G3++;
                        selectedPiece.group = 3;
                        break;
                }
                int newNumber2 = x1 / 6;

                switch (newNumber2) {
                    case 0:
                        G0--;
                        break;
                    case 1:
                        G1--;
                        break;
                    case 2:
                        G2--;
                        break;
                    case 3:
                        G3--;
                        break;
                }
                print(selectedPiece.nombre + " --> " + selectedPiece.group);
                print("G0 " + G0);
                print("G1 " + G1);
                print("G2 " + G2);
                print("G3 " + G3);
                selectedPiece = null;
            }
            else {
                MovePiece(selectedPiece, x2, y2);
                selectedPiece2 = pieces[x2,y2];
                MovePiece(selectedPiece2, x1, y1);
                pieces[x1, y1] = selectedPiece2;
                pieces[x2, y2] = selectedPiece;
                int aux = selectedPiece.group;
                selectedPiece.group = selectedPiece2.group;
                selectedPiece2.group = aux;
                StatusCell(x1, y1);
                startDrag = Vector2.zero;
                selectedPiece = null;
                selectedCell = casillas[x1, y1];
            }
            return;
        }

    }

    public void GenerateBoard() {
        int cont = 0;
        //Generar las casillas vacias
        for (int i = 0; i < ancho; i++) {
            for (int j = 0; j < alto; j++) {
                GenerateCell(i, j, cont);
                cont++;
            }
        }
    }

    public void GenerateCell(int x, int y, int cont) {
        GameObject pr = Instantiate(CasillaPrefab, new Vector3(x, 0, y), Quaternion.identity);
        Casilla c = pr.GetComponent<Casilla>();
        pr.transform.SetParent(ColorCell.transform,false);
        c.GetComponent<Casilla>().numC = cont;

        casillas[x, y] = c;
        c.transform.position = (Vector3.right * x) + (Vector3.forward * y) + boardOffset + casillaOffset;

        //Desactivas el renderer de cada casilla
        StatusCell(x, y);
    }

    public void GeneratePiece(int x, int y, string name, int id) {
        if (pieces[x,y] == null) {
            Prefab = Resources.Load<GameObject>("Sprites/Prefabs/Comp/" + name);
            GameObject go = Instantiate(Prefab) as GameObject;
            go.transform.SetParent(transform);
            Piece p = go.GetComponent<Piece>();
            piecesAux.Add(p);

            int newNumber = x / 6;
            p.id = id;
            p.nombre = name;
            switch (newNumber) {
                case 0:
                    G0++;
                    p.group = 0;
                    break;
                case 1:
                    G1++;
                    p.group = 1;
                    break;
                case 2:
                    G2++;
                    p.group = 2;
                    break;
                case 3:
                    G3++;
                    p.group = 3;
                    break;
            }
            p.id = id;
            p.nombre = name;
            pieces[x, y] = p;
            MovePiece(p, x, y);
            cantidad++;
        }
    }

    public void MovePiece(Piece p, int x, int y) {
        Drag = false;
        p.transform.position = (Vector3.right * x) + (Vector3.forward * y) + boardOffset + pieceOffset;       
    }
    
    public void StatusCell(int x, int y) {
        selectedCell = casillas[x, y];
        selectedCell.ChangeColor(pieces[x, y] == null, casillas, x, y);
        selectedCell.GetComponent<Renderer>().enabled = false;
    }

    public void BreakFree(int x1, int y1) {
        selectedCell = casillas[x1, y1];
        selectedCell.ChangeColor(true, casillas, x1, y1);
        selectedCell.GetComponent<Renderer>().enabled = false;
    }
    
    
    public void Edit(int x, int y, string name) {
        Piece p = pieces[x, y];
        p.nombre = name;
    }

    public void Contabilizar() {
        int G0B = 0;
        int G1B = 0;
        int G2B = 0;
        int G3B = 0;

        for (int i = 0; i < piecesAux.Count; i++) {
            if (piecesAux[i].id >=0 && piecesAux[i].id <= 7) {
                if (piecesAux[i].group == 0) {
                    //print("G0B " + G0B);
                    G0B++;  
                }
            } 
            if (piecesAux[i].id >= 8 && piecesAux[i].id <= 15) {
                if (piecesAux[i].group == 1) {
                    G1B++;
                    //print("G1B " + G1B);
                }
            }
           
            if (piecesAux[i].id >= 16 && piecesAux[i].id <= 23) {
                if (piecesAux[i].group == 2) {
                    G2B++;
                    //print("G2B " + G2B);
                }
            }
            
            if (piecesAux[i].id >= 24 && piecesAux[i].id <= 31) {
                if (piecesAux[i].group == 3) {
                    G3B++;
                    //print("G3B " + G3B);
                }
            }
        }
        int total = G0B + G1B + G2B + G3B;
        RG0.GetComponent<Text>().text = G0B.ToString();
        RG1.GetComponent<Text>().text = G1B.ToString();
        RG2.GetComponent<Text>().text = G2B.ToString();
        RG3.GetComponent<Text>().text = G3B.ToString();
        Total.GetComponent<Text>().text = total.ToString();
        panel.SetActive(true);  
    }

    public void EliminarCartas() {
        button.SetActive(false);
        GameObject g;
        int i;
        for (i = 0; i < tablero.transform.childCount; i++) {
            g = tablero.transform.GetChild(i).gameObject;
            if ((g.name != "Line") && (g.name != "Line2") && (g.name != "Line3") && (g.name != "Line4")) {
                Destroy(g);
            }
           
        }
        
        Resultados();
        cantidad = 0;
    }

  
    public void Resultados() {
        Contabilizar();
        playButton.SetActive(true);
    }

    public void Exit() {
        panel.SetActive(false);
        
            
    }
    public void CleanAux() {
        
        for (int i = 0; i < piecesAux.Count; i++) {
            piecesAux.RemoveAt(i);
        }
    }
}
