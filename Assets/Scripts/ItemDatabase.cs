﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;
using System.IO;

public class ItemDatabase : MonoBehaviour {
    private static List<Item> database = new List<Item>();
    private JsonData itemData;

    void Awake() {
        itemData = JsonMapper.ToObject(File.ReadAllText(Application.dataPath + "/StreamingAssets/Items.json"));
        ConstructItemDatabase();
    }

    void Start() {
        //itemData = JsonMapper.ToObject(File.ReadAllText(Application.dataPath + "/StreamingAssets/Items.json"));
        //ConstructItemDatabase();
        /*print("*** Item 1 ***");
        Debug.Log(database[0].ID);
        Debug.Log(database[0].Title);
        Debug.Log(database[0].Info);*/
    }

    void ConstructItemDatabase() {
        for (int i = 0; i < itemData.Count; i++) {
            database.Add(new Item((int)itemData[i]["id"], itemData[i]["title"].ToString(), itemData[i]["info"].ToString()));
        }
    }

    public Item FetchItemByID(int id) {
        for (int i = 0; i < database.Count; i++) {
            if (database[i].ID == id)
                return database[i];
        }
        return null;
    }
}

    /*public string ReturnInfo(int id) {
        string info = (string)database[id].Info;
        return (info);
    }*/

public class Item {
    public int ID { get; set; }
    public string Title { get; set; }
    public string Info { get; set; }
    //public Sprite Sprite { get; set; }

    public Item(int id, string title, string info) {
        this.ID = id;
        this.Title = title;
        this.Info = info;
        //this.Sprite = Resources.Load<Sprite>("Sprites/Cards/" + id);
    }

    public Item() {
        this.ID = -1;
    }
}