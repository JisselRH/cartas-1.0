﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.UI;

public class GenerarCartas : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {

    public GameObject CartaPrefab, SlotPrefab;
    private int total = 52;
    public GameObject Tablero, Panel;
    public List<GameObject> cartas = new List<GameObject>();
    
    public Sprite[] sprites = new Sprite[52];
    public Sprite sprite, spriteAct;
    public int actual,x,y;
    private bool Drag;
    private Vector2 offset;
    private Vector3 boardOffset = new Vector3(-12f, 0, -12f);
    private Vector2 mouseOver;
    private Inventory inv;

    PointerEventData eventData;

    void Start() {
        Tablero = GameObject.Find("Tablero");
        Panel = GameObject.Find("Inventory Panel");
        inv = GameObject.Find("Inventory").GetComponent<Inventory>();
        actual = 0;
        
    }

    public void OnBeginDrag(PointerEventData eventData) {
        Drag = true;
        print("BeginDrag");
        if (spriteAct != null) {
            offset = eventData.position - new Vector2(this.transform.position.x, this.transform.position.y);
            this.transform.SetParent(this.transform.parent.parent);

            this.transform.position = eventData.position - offset;
            GetComponent<CanvasGroup>().blocksRaycasts = false;

            //obtiene la camara y envia el objeto para ajustar el tamaño
            //GameObject camara = GameObject.Find("Camera");
            //camara.GetComponent<CameraController>().SetItemData(this);
        }

    }
    
    //Arrastrando
    public void OnDrag(PointerEventData eventData) {
        print("OnDrag");
        if (spriteAct != null) {
            this.transform.SetParent(inv.transform);
            this.transform.position = eventData.position - offset;
        }

        RaycastHit hit;

        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 200.0f, LayerMask.GetMask("Tablero"))) {

            mouseOver.x = (int)(hit.point.x - boardOffset.x);
            mouseOver.y = (int)(hit.point.z - boardOffset.z);
        }
        else {
            mouseOver.x = -1;
            mouseOver.y = -1;
        }

        x = (int)mouseOver.x;
        y = (int)mouseOver.y;
        
    }



    public void OnEndDrag(PointerEventData eventData) {
        Drag = false;

      

        this.transform.SetParent(inv.slots[actual].transform);
        this.transform.position = inv.slots[actual].transform.position;

        RaycastHit hit;

        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 200.0f, LayerMask.GetMask("Tablero"))) {

            mouseOver.x = (int)(hit.point.x - boardOffset.x);
            mouseOver.y = (int)(hit.point.z - boardOffset.z);
        }
        else {
            mouseOver.x = -1;
            mouseOver.y = -1;
        }

        x = (int)mouseOver.x;
        y = (int)mouseOver.y;

        //GeneratePieceA(x, y);

        GetComponent<CanvasGroup>().blocksRaycasts = true;
    }
}

